#v1.0.1

- Fixed Webpack configs


#v1.0.0 

   - Added Grunt
   - Installed Sass ``/assets/sass/``
   - Added Composer ``root folder``
   - Added Manifest.json file ``root folder``
   - Added tracking ``/inc/ga-tracking.php``
   - Added Social-Share ``/inc/social-share.php``
   - Added Article Page ``/template-parts/content-post.php``
   - Added Meta Boxes ``/inc/meta-boxes.php``
   - Added filters ``/inc/filters.php``
   - Added Customizer options ``/inc/customizer.php``
    


### Version Bumped files
 - manifest.json
 - package-lock.json
 - package.json