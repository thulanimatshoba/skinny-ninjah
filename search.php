<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Skinny_Ninjah
 */

get_header();
?>

	<section id="primary" class="content-area">
	<div class="breadcrumbs">
			<div class="uk-container uk-padding-small">
				<?php get_template_part( 'partials/header/breadcrumbs' ); ?>
			</div>
		</div>
		<main id="main" class="site-main">
			<div class="uk-container">

				<?php if ( have_posts() ) : ?>

					<header class="page-header">
						<h1 class="page-title">
							<?php
							/* translators: %s: search query. */
							printf( esc_html__( 'Search Results for: %s', 'skinny-ninjah' ), '<span>' . get_search_query() . '</span>' );
							?>
						</h1>
						<p><?php echo sprintf( __( 'Showing %s out of %s results.' ), $wp_query->post_count, $wp_query->found_posts ); ?></p>
					</header><!-- .page-header -->

					<?php
					/* Start the Loop */
					while ( have_posts() ) :
						the_post();

						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						//get_template_part( 'template-parts/content', 'search' );
						get_template_part( 'template-parts/content', get_post_format() );

					endwhile; ?>

					<div class="pagination uk-padding uk-pagination uk-flex-center">
							<?php echo paginate_links( array(
								'prev_text' => __( '←' ),
								'next_text' => __( '→' ),
							) ); ?>
					</div>
				<?php //the_posts_navigation();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>
			</div>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
//get_sidebar();
get_footer();
