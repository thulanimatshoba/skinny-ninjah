jQuery(document).ready(function($){

     revealPosts();

     /** Variable declarations */
     var last_scroll = 0;


    /** Pre Loader Jizz */
    $(document).ready(function () {
        setTimeout(function () {
            $('body').addClass('loaded');
        });
    });

    //Adds that cool shit on the menu when you hover
    $(document).ready(function() {
        $('.main-navigation .damn ').each(function() {
            $(this).attr('data-hover', $(this).text());
        });
    });

    /**  Gallery Post Format Jizz */
    $( ".uk-slidenav svg" ).wrap( "<div class='preview-container'></div>" );
    $(".preview-container").prepend($("<span class='thumbnail-container background-image'></span>"));

    //Ajax load more
    $(document).on('click', '.skinny-ninjah-load-more:not(.loading)', function() {

        var that = $(this);
        var page = that.data('page');
        var newPage = page+1;
        var ajaxurl = that.data('url');
        var prev = that.data('prev');
        var archive = that.data('archive');


        if( typeof prev === 'undefined' ) {
            prev = 0;
        }

        if( typeof archive === 'undefined' ) {
            archive = 0;
        }

        that.addClass('loading').find('.text').slideUp(320);
        that.find('.skinny-ninjah-icon').addClass('spin');

        $.ajax({
            url : ajaxurl,
            type : 'post',
            data : {
                page : page,
                prev : prev,
                archive : archive,
                action : 'skinny_ninjah_load_more'
            },
            error : function( responce ) {
                console.log(responce);
            },
            success : function( responce ) {

                if( responce == 0 ) {
                    $('.skinny-ninjah-posts-container').append( '<div class="uk-text-center"><h3>Damn son, you reached the end of the line</h3><p>No more posts to load.</p></div>' );
                    that.slideUp(320);
                } else {

                    setTimeout(function() {

                        if( prev == 1 ) {
                            $('.skinny-ninjah-posts-container').prepend( responce );
                            newPage = page-1;
                        } else {
                            $('.skinny-ninjah-posts-container').append( responce );
                        }

                        if( newPage == 1 ) {
                            that.slideUp(320);

                        } else {
                            that.data('page', newPage);
                            that.removeClass('loading').find('.text').slideDown(320);
                            that.find('.skinny-ninjah-icon').removeClass('spin');
                        }

                        revealPosts();

                    }, 1000);

                }
            }
        });

    });

    /** Scroll Functions */
    $(window).scroll( function() {
        var scroll = $(window).scrollTop();
        if( Math.abs( scroll - last_scroll ) > $(window).height()*0.1 ) {
            last_scroll = scroll;

            $('.page-limit').each(function( index ) {
                if( isVisible( $(this) ) ) {
                    history.replaceState( null, null, $(this).attr("data-page") );
                    return(false);

                }
            });
        }
    });

    /** Helper Functions */
    function revealPosts() {
        var posts = $('article:not(.reveal)');
        var i = 0;

        setInterval(function() {
            if( i >= posts.length ) return false;
            var el = posts[i];
            $(el).addClass('reveal');
            i++

        }, 200)
    }

    function isVisible( element ) {
        var scroll_pos = $(window).scrollTop();
        var window_height = $(window).height();
        var el_top = $(element).offset().top;
        var el_height = $(element).height();
        var el_bottom = el_top + el_height;
        return ( el_bottom - el_height*0.25 > scroll_pos ) && ( el_top < ( scroll_pos+0.5*window_height ) );

    }

        /** Contact Form Sumbition */
    $('#SkinnyNinjahContactForm').on('submit', function(e) {
        e.preventDefault();

        $('.uk-form-danger').removeClass('uk-form-danger');
        $('.js-show-feedback').removeClass('js-show-feedback');

        var form = $(this);
            name = form.find('#name').val(),
            email = form.find('#email').val(),
            message = form.find('#message').val(),
            ajaxurl = form.data('url');

            if( name === '' ) {
                $('#name').parent('.uk-form-controls').addClass('uk-form-danger');
                $('#name').addClass('uk-form-danger');
                return;
            }

            if( email === '' ) {
                $('#email').parent('.uk-form-controls').addClass('uk-form-danger');
                $('#email').addClass('uk-form-danger');
                return;
            }

            if( message === '' ) {
                $('#message').parent('.uk-form-controls').addClass('uk-form-danger');
                return;
            }

            form.find('input, button, textarea').attr('disabled','disabled');
            $('.js-form-submission').addClass('js-show-feedback');

            $.ajax({
                url : ajaxurl,
                type : 'post',
                 data : {
                    name : name,
                    email : email,
                    message : message,
                    action : 'skinny_ninjah_save_user_contact_form'
                },
                error : function( response ) {
                    $('.js-form-submission').removeClass('js-show-feedback');
                    $('.js-form-error').addClass('js-show-feedback');
                    form.find('input, button, textarea').removeAttr('disabled');
                },
                success : function( response ) {
                    if( response == 0 ) {

                        setTimeout(function() {
                            $('.js-form-submission').removeClass('js-show-feedback');
                            $('.js-form-error').addClass('js-show-feedback');
                            form.find('input, button, textarea').removeAttr('disabled');
                        },2000);
                    } else {

                        setTimeout(function() {
                            $('.js-form-submission').removeClass('js-show-feedback');
                            $('.js-form-success').addClass('js-show-feedback');
                            form.find('input, button, textarea').removeAttr('disabled').val('');
                        },2000);
                    }
                }
        });
    });

    //Smooth Sliding From menu item to another

        $('.go-down a[href*=#]:not([href=#])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    var width = Math.max($(window).width(), window.innerWidth);
                    if (width <= 640) {
                        $('html,body').stop().animate({
                            scrollTop: target.offset().top - 178
                        }, 1000);
                    } else {
                        $('html,body').stop().animate({
                            scrollTop: target.offset().top - 112
                        }, 1000);
                    }
                    return false;
                }
            }
        });

});
