<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Skinny_Ninjah
 */

get_header();
 
?>

	<div id="primary" class="content-area">
    <div class="breadcrumbs">
			<div class="uk-container uk-padding-small">
				<?php get_template_part( 'partials/header/breadcrumbs' ); ?>
			</div>
		</div>
        <main id="main" class="site-main">

            <?php
            while (have_posts()) :
            the_post();
            
            if ( !is_user_logged_in() ) {
                skinny_ninjah_getPostViews(get_the_ID());
            }

            //skinny_ninjah_save_post_views( get_the_ID() );

            get_template_part('template-parts/pages/content', 'post'); ?>

            <div class="uk-container">
                <?php

                the_post_navigation();

                // If comments are open or we have at least one comment, load up the comment template.
                if (comments_open() || get_comments_number()) :
                    comments_template();
                endif;

                endwhile; // End of the loop.
                ?>
            </div>
        </main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
