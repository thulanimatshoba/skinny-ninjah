<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

get_header(); ?>

<div id="primary" class="content-area">
    <div class="breadcrumbs">
		<div class="uk-container uk-padding-small">
			<?php get_template_part( 'partials/header/breadcrumbs' ); ?>
		</div>
	</div>
    <main id="main" class="site-main">

    <header class="archive-header uk-container">
        <?php the_archive_title('<h1 class="page-title">', '</h1>'); ?>
    </header>

    <?php if( is_paged() ) : ?>
        <div class="uk-container uk-text-center uk-padding container-load-previous">
            <a class="skinny-ninjah-load-more uk-button uk-button-default" data-prev="1" data-archive="<?php echo skinny_ninjah_grab_current_url(); ?>" data-page="<?php echo skinny_ninjah_check_paged(1); ?>" data-url="<?php echo admin_url( 'admin-ajax.php');?>">
                <span class="skinny-ninjah-icon skinny-ninjah-spinner9"></span>
                <span class="text uk-float-right uk-margin-small-left"><?php _e( 'Load Previous' ); ?></span>
            </a>
        </div>
    <?php endif; ?>

        <div class="uk-container skinny-ninjah-posts-container">
            <?php
                if( have_posts() ) :
                    echo '<div class="page-limit" data-page="' . $_SERVER["REQUEST_URI"] . '">';
                    while( have_posts() ) : the_post();
                        // $class = 'reveal';
                        // set_query_var( 'post-class', $class );
                        get_template_part( 'template-parts/content', get_post_format() );
                    endwhile;
                    echo '</div>';
                endif;
            ?>
        </div>

        <div class="uk-container uk-text-center uk-padding">
            <a class="skinny-ninjah-load-more uk-button uk-button-default" data-page="<?php echo skinny_ninjah_check_paged(1); ?>" data-archive="<?php echo skinny_ninjah_grab_current_url(); ?>" data-url="<?php echo admin_url( 'admin-ajax.php');?>">
                <span class="skinny-ninjah-icon skinny-ninjah-spinner9"></span>
                <span class="text uk-float-right uk-margin-small-left"><?php _e( 'Load More' ); ?></span>
            </a>
        </div>
    </main>
</div>

<?php get_footer(); ?>
