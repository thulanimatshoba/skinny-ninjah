<?php
	/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

	get_header();
	?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <?php
		if (have_posts()) :

			if (is_home() && !is_front_page()) :
				?>
        <header>
            <h1 class="page-title"><?php single_post_title(); ?></h1>
        </header>
        <?php
	endif; ?>

        <div class="uk-container sn-posts-container">
            <div class="what uk-child-width-expand@s" uk-grid uk-scrollspy="cls: uk-animation-fade; target: > .category-article; delay: 500; repeat: false">

                <?php
				/* Start the Loop */
				while (have_posts()) :
					the_post();

					/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
					get_template_part('template-parts/content', get_post_type());

				endwhile;

			else :

				get_template_part('template-parts/content', 'none');

			endif;
			?>
			</div>

			<div class="uk-container uk-text-center uk-padding">
		<?php //the_posts_navigation(); ?>
		
			<a class="uk-button uk-button-default sn-load-more" data-page="1" data-url="<?php echo admin_url( 'admin-ajax.php');?>">
			<span class="uk-icon uk-margin-small-right" uk-icon="refresh"></span> 
			<span class="sn-load-text"><?php _e('Load more');?></span>
		</a>
		</div>
			
		</div>
		
		
    </main><!-- #main -->
</div><!-- #primary -->
</div>

<?php
 //get_sidebar();
get_footer();
