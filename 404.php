<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Skinny_Ninjah
 */

get_header();
?>

	<div id="primary" class="content-area">
		<div class="breadcrumbs">
			<div class="uk-container uk-padding-small">
				<?php get_template_part( 'partials/header/breadcrumbs' ); ?>
			</div>
		</div>
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				<header class="page-header uk-container">
					<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'skinny-ninjah' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
                    <div class="uk-container uk-text-center uk-padding uk-padding-remove-top">
                        <img src="<?php echo get_template_directory_uri(); ?>/src/images/space-404.gif" alt="skinny-ninjah"/>
					    <p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'skinny-ninjah' ); ?></p>

                        <a class="uk-text-center uk-button uk-button-default" href="#search-modal" uk-search-icon uk-toggle></a>

                        <?php get_template_part('partials/modals/search', 'modal'); ?>
                    </div>


				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
