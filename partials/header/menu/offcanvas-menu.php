<div id="offcanvas-push" uk-offcanvas="mode: push; overlay: true">
    <div class="uk-offcanvas-bar">

        <button class="uk-offcanvas-close" type="button" uk-close></button>

        <?php get_template_part('partials/header/logo'); ?>

        <?php
        if ( !has_nav_menu( 'offcanvas' ) ) {
            return;
        }

        wp_nav_menu(
            array(
                'theme_location' => 'offcanvas',
                'container_class' => 'uk-navbar-nav uk-margin-medium-top',
                'menu_class'    => 'uk-nav'
            ));
        ?>

        <hr class="uk-divider-icon">

        <div class="login-buttons">
            <div class="uk-float-left">
                <?php _e('Follow Us On'); ?>
            </div>
            <div class="uk-float-right">
                <?php get_template_part( 'partials/header/menu/social', 'menu' ); ?>
            </div>
        </div>
    </div>
</div>