<?php
/**
 * Created by PhpStorm.
 * User: thulani-matshoba
 * Date: 2019-03-14
 * Time: 23:21
 */


if (!has_nav_menu('header')) {
    return;
}

wp_nav_menu(
    array(
        'theme_location' => 'header',
        'container_class' => 'ninjah-menu',
        'menu_class' => 'uk-visible@s',
        'link_before' => '<span class="damn">',
        'link_after' => '</span>',
    )
);