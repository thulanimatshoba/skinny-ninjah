<div class="uk-offcanvas-content">

    <header id="masthead" class="site-header" uk-sticky="top: 100; animation: uk-animation-slide-top; bottom: #sticky-on-scroll-up" style="background-image: url(<?php header_image(); ?>); background-repeat: no-repeat; background-size: cover; background-position: center center;">

    <div class="uk-container uk-navbar">
        <div class="header-branding uk-navbar-left">
            <?php get_template_part('partials/header/logo'); ?>
        </div>

        <div class="uk-navbar-right">
            <?php get_template_part('partials/header/menu/social', 'menu' ); ?>
            <div class="search">
                <!-- <a class="" href="#search-modal" uk-search-icon uk-toggle></a> -->
                <?php //get_template_part('template-parts/search/search' ); ?>
                <?php if (! dynamic_sidebar('header-cart') ): endif; ?>
            </div>
        </div>

        <nav id="site-navigation" class="main-navigation uk-navbar-center">
            <button class="uk-navbar-toggle menu-toggle uk-hidden@s" uk-navbar-toggle-icon href="#" uk-toggle="target: #offcanvas-push"></button>
            <?php get_template_part('partials/header/menu/header', 'menu'); ?>
        </nav><!-- #site-navigation -->
    </div>
    </header><!-- #masthead -->

<?php get_template_part( 'partials/header/menu/offcanvas', 'menu' ); ?>