<?php

/**
 * Template part for displaying page content in shop-page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <div class ="uk-container">
		    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </div>
	</header><!-- .entry-header -->

    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-3-4@s">
                <div class="page-content">
                    <?php
                    the_content();

                    wp_link_pages( array(
                        'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'skinny-ninjah' ),
                        'after'  => '</div>',
                    ) );
                    ?>
                </div><!-- .page-content -->
            </div>
            <div class="uk-margin-top uk-width-1-4@s">
                <aside id="primary-sidebar">
                    <?php dynamic_sidebar('secondary-sidebar'); ?>
                </aside>
            </div>
        </div>

        <?php if ( get_edit_post_link() ) : ?>
        <footer class="page-footer">
            <?php
                edit_post_link(
                    sprintf(
                        wp_kses(
                            __( 'Edit <span class="screen-reader-text">%s</span>', 'skinny-ninjah' ),
                            array(
                                'span' => array(
                                'class' => array(),
                                ),
                            )
                        ),
                        get_the_title()
                    ),
                    '<span class="edit-link">',
                    '</span>'
                );
            ?>
        </footer><!-- .page-footer -->
        <?php endif; ?>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->
