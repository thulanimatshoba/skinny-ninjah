<?php

/**
 * Template Name: Shop Page
 *
 * This is the template that displays The Shop Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

get_header();

?>

    <div id="primary" class="content-area">
        <div class="breadcrumbs">
			<div class="uk-container uk-padding-small">
				<?php get_template_part( 'partials/header/breadcrumbs' ); ?>
			</div>
		</div>
        <main id="main" class="site-main">
                <?php
                while ( have_posts() ) :
                    the_post();
                    get_template_part( 'template-parts/content', 'shop' );
                endwhile; // End of the loop.
                ?>
        </main><!-- #main -->
    </div><!-- #primary -->
<?php
get_footer();