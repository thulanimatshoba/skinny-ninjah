<?php
/**
 * Template part for displaying Aside Post Format
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

?>


<article id="post-<?php the_ID(); ?>" <?php post_class('skinny-ninjah-aside-format'); ?>>

    <div class="aside-container">
        <div class="skinny-grid" uk-grid>
            <div class="uk-width-1-5@m">
                <?php if( skinny_ninjah_get_attachment() ) : ?>
                    <div class="aside-featured background-image" style="background-image: url(<?php echo skinny_ninjah_get_attachment(); ?>);"></div>
                <?php endif; ?>
            </div>
            <div class="uk-width-expand@m">
                <header class="entry-header">
                    <div class="entry-meta">
                        <?php echo skinny_ninjah_posted_meta(); ?>
                    </div>
                </header>

                <div class="entry-content">
                    <div class="entry-excerpt">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>

        <footer class="entry-footer">
            <div class="skinny-grid" uk-grid>
                <div class="uk-width-1-5@m"></div>
                <div class="uk-width-expand@m">
                    <?php echo skinny_ninjah_posted_footer(); ?>
                </div>
            </div>
        </footer>
    </div>


</article>
