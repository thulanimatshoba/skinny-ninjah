<?php
/**
 * Template part for displaying Link Post Format
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'skinny-ninjah-link-format' ); ?>>
    <header class="entry-header">
        <?php
            $link = skinny_ninjah_grab_url();
            the_title( '<h1 class="entry-title"><a href="'. $link . '" target="_blank">', '<div class="link-icon"><span class="skinny-ninjah-icon skinny-ninjah-link"></span></div></a></h1>' );
            ?>
            <div class="entry-meta">
                <?php echo skinny_ninjah_posted_meta(); ?>
            </div>
    </header>


</article>
