<?php
/**
 * Template part for displaying Gallery Post Format
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('skinny-ninjah-gallery-format'); ?>>
    <header class="entry-header">

    <?php if( skinny_ninjah_get_attachment() ): ?>

        <div id="post-gallery-<?php the_ID(); ?>" class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="clsActivated: uk-transition-active; center: true; autoplay: true">

            <ul class="uk-slider-items uk-grid">
            <?php
                $attachments = skinny_ninjah_get_bs_slides( skinny_ninjah_get_attachment(7) );
                foreach( $attachments as $attachment ):
                ?>

                <li class="uk-width-3-4 standard-featured background-image <?php echo $attachment['class']; ?>" style="background-image: url( <?php echo $attachment['url']; ?> );">
                <div class="uk-overlay background-gradient uk-position-bottom uk-text-center uk-transition-slide-bottom">
                    <div class="entry-excerpt image-caption background-gradient">
                        <p><?php echo $attachment['caption']; ?></p>
                    </div>
                </div>
            </li>
                <?php endforeach; ?>
            </ul>

            <a class="uk-position-center-left uk-position-small uk-hidden-hover text-shadow" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small uk-hidden-hover text-shadow" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>

        <?php endif; ?>


        <?php the_title( '<h1 class="entry-title"><a href="'. esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' ); ?>
            <div class="entry-meta">
                <?php echo skinny_ninjah_posted_meta(); ?>
            </div>
    </header>

    <div class="entry-content">

        <div class="entry-excerpt">
            <?php the_excerpt(); ?>
        </div>

         <div class="button-container">
            <a href="<?php the_permalink(); ?>" class="uk-button uk-button-default">
                <?php _e( 'Read More' ); ?>
            </a>
        </div>
    </div>

    <footer class="entry-footer">
        <?php echo skinny_ninjah_posted_footer(); ?>
    </footer>


</article>
