<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

?>

<article id="post-<?php the_ID(); ?>" <?php ($c === 1 ? post_class('uk-width-2-3@s featured-post') : post_class('category-article uk-width-1-3@s')); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php
				skinny_ninjah_posted_by();
                skinny_ninjah_posted_on();
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php skinny_ninjah_post_thumbnail(); ?>

	<div class="entry-content">
        <div class="uk-container">
            <?php echo tm_length_excerpt('');

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'skinny-ninjah' ),
			'after'  => '</div>',
		) );
		?>
        </div>
	</div><!-- .entry-content -->


</article><!-- #post-<?php the_ID(); ?> -->
