<?php
/**
 * Created by PhpStorm.
 * User: thulani-matshoba
 * Date: 2019-03-17
 * Time: 21:40
 */
?>

<?php
/**
 * Template Name: Home Page
 *
 * This is the template that the home page.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

get_header();
$backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
?>

<div class="header-wrap" style="background: url('<?php echo $backgroundImg[0]; ?>') no-repeat; height: 666px; background-size: cover;">

</div>
    <div id="primary" class="content-area">
        <main id="main" class="site-main">

            <?php
            while ( have_posts() ) :
                the_post();

                get_template_part( 'template-parts/pages/content', 'home' );

            endwhile; // End of the loop.
            ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();