<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( '' ); ?>>
    <header class="entry-header">
        <?php the_title( '<h1 class="entry-title"><a href="'. esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' ); ?>
            <div class="entry-meta">
                <?php echo skinny_ninjah_posted_meta(); ?>
            </div>
    </header>

    <div class="entry-content">
        <?php if( skinny_ninjah_get_attachment() ) :
            $featured_image = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ); ?>
            <a class="standard-featured-link" href="<?php the_permalink(); ?>">
                <div class="standard-featured background-image" style="background-image: url(<?php echo skinny_ninjah_get_attachment(); ?>);"></div>
            </a>
        <?php endif; ?>

        <div class="entry-excerpt">
            <?php the_excerpt(); ?>
        </div>

         <div class="button-container">
            <a href="<?php the_permalink(); ?>" class="uk-button uk-button-default">
                <?php _e( 'Read More' ); ?>
            </a>
        </div>
    </div>

    <footer class="entry-footer">
        <?php echo skinny_ninjah_posted_footer(); ?>
    </footer>


</article>
