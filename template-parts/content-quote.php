<?php
/**
 * Template part for Quote Post Format
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'skinny-ninjah-quote-format' ); ?>>
    <header class="entry-header">
        <h1 class="quote-content"><a href="<?php the_permalink(); ?>"><?php echo get_the_content(); ?></a></h1>
        <?php the_title( '<h2 class="quote-author">- ', ' -</h2>' ); ?>
    </header>

    <footer class="entry-footer">
        <?php echo skinny_ninjah_posted_footer(); ?>
    </footer>


</article>
