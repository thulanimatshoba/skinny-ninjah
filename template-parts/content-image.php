<?php
/**
 * Template part for displaying Image Post Format
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('skinny-ninjah-image-format'); ?>>

        <header class="entry-header text-shadow background-image" style="background-image: url(<?php echo skinny_ninjah_get_attachment(); ?>);">
            <?php the_title( '<h1 class="entry-title"><a href="'. esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' ); ?>
                <div class="entry-meta">
                    <?php echo skinny_ninjah_posted_meta(); ?>
                </div>

                <div class="entry-excerpt image-caption background-gradient">
                    <?php the_excerpt(); ?>
                </div>
        </header>

    <footer class="entry-footer">
        <?php echo skinny_ninjah_posted_footer(); ?>
    </footer>


</article>
