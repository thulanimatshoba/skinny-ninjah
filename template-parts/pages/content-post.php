<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
        <div class="uk-container">
            <div uk-grid>
                <div class="uk-width-3-4@m">

                    <header class="entry-header">
                            <h3 class="uk-h2">
                                <?php //skinny_ninjah_title(50); ?>
                                <?php the_title(); ?>
                            </h3>
                    </header><!-- .entry-header -->

                    <?php skinny_ninjah_post_thumbnail(); ?>
                    <div uk-grid>
                        <div class="uk-width-1-3@m">
                            <div class="uk-card uk-card-default uk-card-body">
                                <?php if ( 'post' === get_post_type() ) : ?>
                                <div class="entry-meta">
                                    <?php
                                    skinny_ninjah_posted_by();
                                    skinny_ninjah_posted_on();
                                    ?>
                                </div><!-- .entry-meta -->
                                    <div class="uk-margin-top">
                                        <?php get_template_part('includes/social-share'); ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="uk-width-expand@m">
                            <div class="uk-margin-top">
                                <?php
                                the_content( sprintf(
                                    wp_kses(
                                    /* translators: %s: Name of current post. Only visible to screen readers */
                                        __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'skinny-ninjah' ),
                                        array(
                                            'span' => array(
                                                'class' => array(),
                                            ),
                                        )
                                    ),
                                    get_the_title()
                                ) );

                                wp_link_pages( array(
                                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'skinny-ninjah' ),
                                    'after'  => '</div>',
                                ) );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-4@m">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
        <div class="uk-container uk-padding">
		    <?php skinny_ninjah_entry_footer(); ?>
        </div>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
