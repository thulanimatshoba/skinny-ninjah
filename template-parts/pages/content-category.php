<?php
/**
 * Created by PhpStorm.
 * User: thulani-matshoba
 * Date: 2019-03-20
 * Time: 17:07
 */
?>

<div class="uk-child-width-expand@s" uk-grid uk-scrollspy="cls: uk-animation-fade; target: > .category-article; delay: 500; repeat: false">

    <?php
    $cat_id = get_query_var('cat') ? get_query_var('cat') : get_the_category()[0]->term_id;

    $c = 0;

    $args = array(
        'order' => 'DESC',
        'orderby' => 'date',
        'cat' => $cat_id
    );

    $category = new WP_Query($args);

    if ($category->have_posts()) {
    while ($category->have_posts()) {
        $category->the_post();

        $c++; ?>


        <div id="post-<?php echo get_the_ID(); ?>" <?php ($c === 1 ? post_class('category-article uk-width-2-3@s featured-post') : post_class('category-article uk-width-1-3@s')); ?>>
            <div class="post-thumbnail">
                <a href="<?php the_permalink(); ?>">
                    <?php (has_post_thumbnail() ? the_post_thumbnail('featured-featured') : '<img src="' . get_stylesheet_directory() . '/src/images/ninjah.png" />'); ?>
                </a>

                <div class="uk-article-meta post-date">
                    <i class="fab fa-accessible-icon"></i>&nbsp;&nbsp;<?php the_time('jS F Y') ?>
                </div>
            </div>

            <div class="uk-post-content">

                <h3 class="uk-margin-small-top">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php skinny_ninjah_title(60); ?>
                    </a>
                </h3>

                <div class="cat-excerpt">
                    <?php echo tm_length_excerpt('30'); ?>
                </div>

            </div>
            <?php if (is_active_sidebar('secondary-sidebar')) {
                $post_counter = (!isset($post_counter) || $post_counter === null) ? 1 : ++$post_counter;
                $tf_dfp_config_count = 2; //Make configurable
                if ($post_counter === $tf_dfp_config_count) {
                    dynamic_sidebar('secondary-sidebar');
                }
            } ?>
        </div>


        <?php

    } ?>
</div>
<?php skinny_ninjah_pagination();?>
    </div>
<?php

//wp_reset_postdata();

} else {
    echo "<div class='uk-width-medium-1-1 no-posts-found'>";
    echo "<p>" . __('No posts found.', 'skinny-ninjah') . "</p>";
    echo "</div>";
} ?>
