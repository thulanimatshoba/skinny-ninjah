<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

            <header class="page-header">
                <div class="uk-container">
                    <?php
                    single_cat_title('<h1 class="uk-modal-title">', '</h1>');
                    the_archive_description('<div class="archive-description uk-text-muted uk-text-small">', '</div>');
                    ?>
                </div>
            </header><!-- .page-header -->

            <div class="uk-container">
                <?php get_template_part('template-parts/pages/content', 'category'); ?>
            </div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
