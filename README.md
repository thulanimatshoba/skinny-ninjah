[![Build Status](https://travis-ci.org/Automattic/_s.svg?branch=master)](https://bitbucket.org/thulanimatshoba/skinny-ninjah/addon/pipelines/home#!/results/2)

_s
===

Hi. I'm the legendary theme developed by Thulani Matshoba and he named me `skinny-ninjah`, or `Skinny Ninjah`, I'm a theme meant for hacking so don't use me as a Parent Theme. Instead try turning me into the next, most awesome, WordPress theme out there. That's what I'm here for.

My ultra-minimal CSS might make me look like theme tartare but that means less stuff to get in your way when you're designing your awesome theme. Here are some of the other more interesting things you'll find here:

* A just right amount of lean, well-commented, modern, HTML5 templates.
* A helpful 404 template.
* A custom header implementation in `inc/custom-header.php` just add the code snippet found in the comments of `inc/custom-header.php` to your `header.php` template.
* Custom template tags in `inc/template-tags.php` that keep your templates clean and neat and prevent code duplication.
* Some small tweaks in `inc/template-functions.php` that can improve your theming experience.
* A script at `js/navigation.js` that makes your menu a toggled dropdown on small screens (like your phone), ready for CSS artistry. It's enqueued in `functions.php`.
* 2 sample CSS layouts in `layouts/` for a sidebar on either side of your content.
Note: `.no-sidebar` styles are not automatically loaded.
* Smartly organized starter CSS in `style.css` that will help you to quickly get your design off the ground.
* Licensed under GPLv2 or later. :) Use it to make something cool.

Getting Started
---------------

If you want to keep it simple, head over to http://thulanimatshoba.co.za and ask `Thulani Matshoba` for some assistance.


1. run `'npm i && grunt'` (inside your theme directory obviously, duh!)
                                or
1. run `'npm i && npm run build'` (inside your theme directory obviously, duh!)

Now you're ready to go! The next step is easy to say, but harder to do: Do some cool shit. :)

Good luck!
