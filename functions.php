<?php
/**
 * Skinny Ninjah functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Skinny_Ninjah
 */

/**
 * Version Number
 */
const THULANI_VER = '1.0.1'; 
const THULANI_UIKIT_VER = '3.0.0';


/**
 * Mobile Detect.
 */
//require get_template_directory() . '/includes/vendor/Mobile_Detect.php';

/**
 * Implement the Assets
 */
require get_template_directory() . '/includes/assets.php';

/**
 * Recent Posts Class
 */
require get_template_directory() . '/includes/classes/Skinny_Ninjah_Recent_Posts.php';

/**
 * Implement Filters
 */
require get_template_directory() . '/includes/filters.php';

/**
 * Implement Ajax
 */
require get_template_directory() . '/includes/ajax.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/includes/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/includes/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/includes/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/includes/customizer.php';

/**
 * Function Admin
 */
require get_template_directory() . '/includes/function-admin.php';

/**
 * Shortcodes
 */
require get_template_directory() . '/includes/shortcodes.php';

/**
 * Widgets
 */
require get_template_directory() . '/includes/widgets/widgets.php';

/**
 * Meta Boxes
 */
require get_template_directory() . '/includes/meta-boxes.php';

/**
 * Post Meta
 */
require get_template_directory() . '/includes/post-meta.php';

/**
 * Post Types
 */
require get_template_directory() . '/includes/post-types.php';

/**
 * Tracking
 */
require get_template_directory() . '/includes/ga-tracking.php';

/**
 * TGM Plugin activation
 */
require get_template_directory() . '/includes/class-tgm-plugin-activation.php';
//require get_template_directory() . '/includes/tgm-plugin-activation.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/includes/jetpack.php';
}
