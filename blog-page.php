<?php
/**
 * Created by PhpStorm.
 * User: thulani-matshoba
 * Date: 2019-03-17
 * Time: 21:37
 */
?>

<div id="main-stories">

    <h1 class="entry-title">Latest News</h1>
    <div id="home-main-news-wrapper" class="clear home-main-news-wrapper">
        <?php
        if (is_page()) {
            $cat=get_cat_ID('Blog'); //use page title to get a category ID
            $posts = get_posts ("cat=$cat&showposts=3");

            if ($posts) {
                foreach ($posts as $post):
                    setup_postdata($post); ?>
                    <div class="other-story">
                        <div class="other-story-header">
                            <div class="other-story-category">
                                <?php
                                $category=get_cat_ID('Blog');
                                ?>
                            </div>
                            <?php if ( function_exists( 'add_theme_support' ) ) {
                                if ( has_post_thumbnail() ) { ?>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(''); ?></a>
                                <?php }
                            } ?>
                        </div>

                        <div class="other-story-content">
                            <div class="the-title">
                                <?php if (strlen(the_title('','',FALSE)) > 30) {
                                    $title_short = substr(the_title('','',FALSE), 0, 30);
                                    preg_match('/^(.*)\s/s', $title_short, $matches);
                                    if ($matches[1]) $title_short = $matches[1];
                                    $title_short = $title_short.' ...read more >';
                                } else {
                                    $title_short = the_title('','',FALSE);
                                } ?>

                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><h1><?php echo $title_short ?></h1></a>
                            </div>

                            <div class="clear">
                                <?php
                                $content = get_the_content();
                                $trimmed_content = wp_trim_words( $content, 40, '<a class="read-more" href="'. get_permalink() .'"> more [+]</a>' );
                                echo"<p>$trimmed_content</p>";

                                ?>
                            </div>
                            <div class="meta">
                                <i class="fa fa-calendar"></i><?php the_time('jS F Y') ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach;
            }
        }
        ?>
    </div>
</div>
