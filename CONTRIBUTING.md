## Version Bumps
Modify the following files:

 * Updated constant ``VER`` in ``functions.php``
 * manifest.json
 * package.json
 * package-lock.json
 * style.css
 * run ``grunt version`` to version bump files
 * run ``grunt`` to recompile assets that have the version within them