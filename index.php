<?php
	/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Skinny_Ninjah
 */

get_header(); ?>

    <div id="primary" class="content-area">
      <div class="breadcrumbs">
        <div class="uk-container uk-padding-small">
          <?php get_template_part( 'partials/header/breadcrumbs' ); ?>
        </div>
      </div>
        <main id="main" class="site-main">

        <?php if( is_paged() ) : ?>
            <div class="uk-container uk-text-center uk-padding container-load-previous">
                <a class="skinny-ninjah-load-more uk-button uk-button-default" data-prev="1" data-page="<?php echo skinny_ninjah_check_paged(1); ?>" data-url="<?php echo admin_url( 'admin-ajax.php');?>">
                    <span class="skinny-ninjah-icon skinny-ninjah-spinner9"></span>
                    <span class="text uk-float-right uk-margin-small-left"><?php _e( 'Load Previous' ); ?></span>
                </a>
            </div>
        <?php endif; ?>

            <div class="uk-container skinny-ninjah-posts-container">
                <?php
                    if( have_posts() ) :
                        echo '<div class="page-limit" data-page="/' . skinny_ninjah_check_paged() . '">';
                        while( have_posts() ) : the_post();

                            get_template_part( 'template-parts/content', get_post_format() );
                        endwhile;
                        echo '</div>';
                    endif;
                ?>
            </div>

            <div class="uk-container uk-text-center uk-padding">
                <a class="skinny-ninjah-load-more uk-button uk-button-default" data-page="<?php echo skinny_ninjah_check_paged(1); ?>" data-url="<?php echo admin_url( 'admin-ajax.php');?>">
                    <span class="skinny-ninjah-icon skinny-ninjah-spinner9"></span>
                    <span class="text uk-float-right uk-margin-small-left"><?php _e( 'Load More' ); ?></span>
                </a>
            </div>
        </main>
    </div>

    <?php get_footer(); ?>
