<?php

/*

@package skinny-ninjah

===============================
Admin Page
===============================
*/


function skinny_ninjah_add_admin_page() {
	
	//Generate skinny_ninjah Admin Page
	add_menu_page( 'Skinny Ninjah Theme Options', 'Skinny Ninjah', 'manage_options', 'skinny_ninjah', 'skinny_ninjah_theme_create_page', get_template_directory_uri() . '/src/images/sn-theme-icon.png', 110 );
	
	//Generate skinny_ninjah Admin Sub Pages
    add_submenu_page( 'skinny_ninjah', 'Skinny Ninjah Sidebar Options', 'Sidebar', 'manage_options', 'skinny_ninjah', 'skinny_ninjah_theme_create_page' );
    add_submenu_page( 'skinny_ninjah', 'Skinny Ninjah Theme Options', 'Theme Options', 'manage_options', 'skinny_ninjah_theme', 'skinny_ninjah_theme_support_page');
	add_submenu_page( 'skinny_ninjah', 'Skinny Ninjah Contact Form', 'Contact Form', 'manage_options', 'skinny_ninjah_theme_contact', 'skinny_ninjah_contact_form_page');
	add_submenu_page( 'skinny_ninjah', 'Skinny Ninjah CSS Options', 'Custom CSS', 'manage_options', 'skinny_ninjah_css', 'skinny_ninjah_theme_settings_page');
	
}
add_action( 'admin_menu', 'skinny_ninjah_add_admin_page' );

//Activate custom settings
	add_action( 'admin_init', 'skinny_ninjah_custom_settings' );

function skinny_ninjah_custom_settings() {
    //Sidebar Options
	register_setting( 'skinny-ninjah-settings-group', 'profile_picture' );
	register_setting( 'skinny-ninjah-settings-group', 'first_name' );
    register_setting( 'skinny-ninjah-settings-group', 'last_name' );
    register_setting( 'skinny-ninjah-settings-group', 'user_description' );
    register_setting( 'skinny-ninjah-settings-group', 'twitter_handler', 'skinny_ninjah_sanitize_twitter_handler' );
    register_setting( 'skinny-ninjah-settings-group', 'facebook_handler' );
    register_setting( 'skinny-ninjah-settings-group', 'instagram_handler' );

    add_settings_section( 'skinny-ninjah-sidebar-options', 'Sidebar Option', 'skinny_ninjah_sidebar_options', 'skinny_ninjah');
    
	add_settings_field( 'sidebar-profile-picture', 'Profile Picture', 'skinny_ninjah_sidebar_profile', 'skinny_ninjah', 'skinny-ninjah-sidebar-options');
	add_settings_field( 'sidebar-name', 'Full Name', 'skinny_ninjah_sidebar_name', 'skinny_ninjah', 'skinny-ninjah-sidebar-options');
	add_settings_field( 'sidebar-description', 'Description', 'skinny_ninjah_sidebar_description', 'skinny_ninjah', 'skinny-ninjah-sidebar-options');
	add_settings_field( 'sidebar-twitter', 'Twitter handler', 'skinny_ninjah_sidebar_twitter', 'skinny_ninjah', 'skinny-ninjah-sidebar-options');
	add_settings_field( 'sidebar-facebook', 'Facebook handler', 'skinny_ninjah_sidebar_facebook', 'skinny_ninjah', 'skinny-ninjah-sidebar-options');
    add_settings_field( 'sidebar-instagram', 'Instagram handler', 'skinny_ninjah_sidebar_instagram', 'skinny_ninjah', 'skinny-ninjah-sidebar-options');
    
    //Theme Support Options
    register_setting( 'skinny-ninjah-theme-support', 'post_formats' );
    register_setting( 'skinny-ninjah-theme-support', 'custom_background' );
    register_setting( 'skinny-ninjah-theme-support', 'custom_header' );

    add_settings_section( 'skinny-ninjah-theme-options', 'Theme Options', 'skinny_ninjah_theme_options', 'skinny_ninjah_theme');

    add_settings_field( 'post-formats', 'Post Formats', 'skinny_ninjah_post_formats', 'skinny_ninjah_theme', 'skinny-ninjah-theme-options');
    add_settings_field( 'custom-background', 'Custom Background', 'skinny_ninjah_custom_background', 'skinny_ninjah_theme', 'skinny-ninjah-theme-options');
    add_settings_field( 'custom-header', 'Custom Header', 'skinny_ninjah_custom_header', 'skinny_ninjah_theme', 'skinny-ninjah-theme-options');

    //Contact Form Options
    register_setting( 'skinny-ninjah-contact-options', 'activate_contact' );

    add_settings_section( 'skinny-ninjah-contact-section', 'Contact Form', 'skinny_ninjah_contact_section', 'skinny_ninjah_theme_contact' );

    add_settings_field( 'activate-form', 'Activate Contact Form', 'skinny_ninjah_activate_contact', 'skinny_ninjah_theme_contact', 'skinny-ninjah-contact-section' );

    //Custom CSS Options
    register_setting( 'skinny-ninjah-custom-css-options', 'skinny_ninjah_css', 'skinny_ninjah_sanitize_custom_css' );

    add_settings_section( 'skinny-ninjah-custom-css-section', 'Custom CSS', 'skinny_ninjah_custom_css_section_callback', 'skinny_ninjah_css'  );

    add_settings_field( 'custom-css', 'Insert your Custom CSS', 'skinny_ninjah_custom_css_callback', 'skinny_ninjah_css', 'skinny-ninjah-custom-css-section' );

}

function skinny_ninjah_custom_css_section_callback() {
    echo 'Customize your theme with your own css';
}

function skinny_ninjah_custom_css_callback() {
    $css = get_option( 'skinny_ninjah_css' );
    $css = ( empty($css) ? '/* Skinny Ninjah Custom CSS */' : $css );
    echo '<div id="customCss">'.$css.'</div><textarea id="skinny_ninjah_css" name="skinny_ninjah_css" style="display:none;visible:hidden;">'.$css.'</textarea>';    
}

function skinny_ninjah_theme_options() {
    echo 'Activate and Deactivate specific Theme Support Options';
}

function skinny_ninjah_contact_section() {
    echo 'Activate and Deactivate the Built-in Contact Form';
}

function skinny_ninjah_activate_contact() {
    $options = get_option( 'activate_contact' );
    $checked = ( @$options == 1 ? 'checked' : '' );
    echo '<label><input type="checkbox" id="activate_contact" name="activate_contact" value="1" '.$checked.' /><br>';    
}

function skinny_ninjah_post_formats() {
    $options = get_option( 'post_formats' );
    $formats = array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' );
    $output = '';
    foreach ( $formats as $format ) {
        $checked = ( @$options[$format] == 1 ? 'checked' : '' );
        $output .= '<label><input type="checkbox" id="'.$format.'" name="post_formats['.$format.']" value="1" '.$checked.' /> '.$format.'</label><br>';    
    }
    echo $output;
}

function skinny_ninjah_custom_background() {
    $options = get_option( 'custom_background' );
    $checked = ( @$options == 1 ? 'checked' : '' );
    echo '<label><input type="checkbox" id="custom_background" name="custom_background" value="1" '.$checked.' /> Activate the Custom Background</label><br>';    
}

function skinny_ninjah_custom_header() {
    $options = get_option( 'custom_header' );
    $checked = ( @$options == 1 ? 'checked' : '' );
    echo '<label><input type="checkbox" id="custom_header" name="custom_header" value="1" '.$checked.' /> Activate the Custom Header</label><br>';    
}

//Sidebar Options
function skinny_ninjah_sidebar_options() {
	echo 'Customize your Sidebar Information';
}

function skinny_ninjah_sidebar_profile() {
    $picture = esc_attr( get_option( 'profile_picture' ) );
    if( empty($picture) ) {
        echo '<input type="button" class="button button-secondary" value="Upload Profile Picture" id="upload-button"><input type="hidden" id="profile-picture" name="profile_picture" value="" />';
    } else {
        echo '<input type="button" class="button button-secondary" value="Replace Profile Picture" id="upload-button"><input type="hidden" id="profile-picture" name="profile_picture" value="'.$picture.'" /> <input type="button" class="button button-secondary" value="Remove" id="remove-picture">';
    }
}

function skinny_ninjah_sidebar_name() {
	$firstName = esc_attr( get_option( 'first_name' ) );
	$lastName = esc_attr( get_option( 'last_name' ) );
	echo '<input type="text" name="first_name" value="'.$firstName.'" placeholder="Last Name" /> <input type="text" name="last_name" value="'.$lastName.'" placeholder="Last Name" />';
}

function skinny_ninjah_sidebar_description() {
    $description = esc_attr( get_option( 'user_description' ) );
    echo '<input type="text" name="user_description" value="'.$description.'" placeholder="Description" /><p class="description">Write something smart bro.</p>'; 
}

function skinny_ninjah_sidebar_twitter() {
    $twitter = esc_attr( get_option( 'twitter_handler' ) );
    echo '<input type="text" name="twitter_handler" value="'.$twitter.'" placeholder="Twitter handler" /><p class="description">Input your Twitter username without the @ character.</p>'; 
}

function skinny_ninjah_sidebar_facebook() {
    $facebook = esc_attr( get_option( 'facebook_handler' ) );
    echo '<input type="text" name="facebook_handler" value="'.$facebook.'" placeholder="Facebook handler" />'; 
}

function skinny_ninjah_sidebar_instagram() {
    $instagram = esc_attr( get_option( 'instagram_handler' ) );
    echo '<input type="text" name="instagram_handler" value="'.$instagram.'" placeholder="Instagram handler" />'; 
}

//Sanitization settings
function skinny_ninjah_sanitize_twitter_handler( $input ) {
    $output = sanitize_text_field( $input ); 
    $output = str_replace('@', '', $output);
        return $output;
}

function skinny_ninjah_sanitize_custom_css( $input ) {
    $output = esc_textarea( $input );
        return $output;
}

//Template submenu functions
function skinny_ninjah_theme_create_page() {
	require_once( get_template_directory() . '/includes/templates/skinny-ninjah-admin.php' );
}

function skinny_ninjah_theme_support_page() {
	require_once( get_template_directory() . '/includes/templates/skinny-ninjah-theme-support.php' );
}

function skinny_ninjah_contact_form_page() {
    require_once( get_template_directory() . '/includes/templates/skinny-ninjah-contact-form.php' );
}

function skinny_ninjah_theme_settings_page() {
	require_once( get_template_directory() . '/includes/templates/skinny-ninjah-custom-css.php' );
	
}