<h1>Skinny Ninjah Contact Form</h1>
<?php settings_errors(); ?>


<p><?php _e( 'Use this shortcode to activate the Contact Form inside a post or page' ); ?></p>
<code>[contact_form]</code>

<form method="post" action="options.php" class="sn-general-form">
	<?php settings_fields( 'skinny-ninjah-contact-options' ); ?>
	<?php do_settings_sections( 'skinny_ninjah_theme_contact' ); ?>
	<?php submit_button(); ?>
</form> 

