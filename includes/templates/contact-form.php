<form id="SkinnyNinjahContactForm" action="#" class="uk-form-stacked skinny-ninjah-contact-form" data-url="<?php echo admin_url('admin-ajax.php');  ?>">
    <div class="form-group uk-margin">
        <div class="uk-form-controls">
            <input type="text" class="uk-input ninjah-input" placeholder="Your Name" id="name" name="name" >
            <small class="uk-form-danger form-error-msg" value="form-danger">Your Name is Required</small>
        </div>
    </div>

    <div class="form-group uk-margin">
        <div class="uk-form-controls">
            <input type="email" class="uk-input ninjah-input" placeholder="Your Email" id="email" name="email" >
            <small class="uk-form-danger form-error-msg" value="form-danger">Your Email is Required</small>
        </div>
    </div>

    <div class="form-group uk-margin">
        <div class="uk-form-controls">
            <textarea placeholder="Your Message" id="message" class="uk-textarea ninjah-input" name="message" ></textarea>
            <small class="uk-form-danger form-error-msg" value="form-danger">Your Message is Required</small>
        </div>
    </div>

    <button type="submit" class="uk-button uk-button-default ninjah-button uk-position-bottom-center">Submit</button>
    <small class="uk-text-primary form-error-msg js-form-submission">Submission in process, please wait...</small>
    <small class="uk-text-success form-error-msg js-form-success">Message successfully submitted, thank you</small>
    <small class="uk-text-danger form-error-msg js-form-error">There was a problem with the contact form, please try again!</small>


</form>