<h1>Skinny Ninjah Sidebar Options</h1>
<?php settings_errors(); ?>
<?php 
    $picture = esc_attr( get_option( 'profile_picture' ) );
    $firstName = esc_attr( get_option( 'first_name' ) );
    $lastName = esc_attr( get_option( 'last_name' ) );
    $fullName = $firstName . ' ' . $lastName;
    $description = esc_attr( get_option( 'user_description' ) );
?>

<div class="sn-sidebar-preview">
    <div class="sn-sidebar">
        <div class="image-container">
            <div id="profile-picture-preview" class="profile-picture" style="background-image: url(<?php print $picture; ?>);"></div>
        </div>
        <h1 class="sn-username"><?php print $fullName; ?></h1>
        <h2 class="sn-description"><?php print $description; ?></h2>
        <div class="icons-wrapper">

        </div>
    </div>
</div>

<form method="post" action="options.php" class="sn-general-form">
	<?php settings_fields( 'skinny-ninjah-settings-group' ); ?>
	<?php do_settings_sections( 'skinny_ninjah' ); ?>
	<?php submit_button( 'Save Changes', 'primary', 'btnSubmit' ); ?>
</form>