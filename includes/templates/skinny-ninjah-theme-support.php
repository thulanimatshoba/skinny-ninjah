<h1>Skinny Ninjah Theme Support</h1>
<?php settings_errors(); ?>


<form method="post" action="options.php" class="sn-general-form">
	<?php settings_fields( 'skinny-ninjah-theme-support' ); ?>
	<?php do_settings_sections( 'skinny_ninjah_theme' ); ?>
	<?php submit_button(); ?>
</form>