<h1>Skinny Ninjah Custom CSS</h1>
<?php settings_errors(); ?>


<form method="post" action="options.php" class="sn-general-form">
	<?php settings_fields( 'skinny-ninjah-custom-css-options' ); ?>
	<?php do_settings_sections( 'skinny_ninjah_css' ); ?>
	<?php submit_button(); ?>
</form>