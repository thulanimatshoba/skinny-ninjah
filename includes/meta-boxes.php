<?php
/**
 * Created by PhpStorm.
 * User: thulani-matshoba
 * Date: 2019-03-15
 * Time: 14:35
 */

add_action( 'cmb2_admin_init', 'cmb2_skinny_ninjah_metaboxes');
/**
 * Define the meta-box and field configurations
 */

function cmb2_skinny_ninjah_metaboxes() {
    //start with an underscore to hide fields from custom fields list
    $prefix = 'skinny_nininjah_';

    //Initiate the metabox
    $cmb = new_cmb2_box( array(
        'id'    =>  'video_metabox',
        'title' =>  __( 'Metabox', 'skinny_ninjah' ),
        'object_types'  =>  array( 'post' ), //Post type
        'context'   =>  'normal',
        'priority'  =>  'high',
        'show_names'    => true,
    ));

    //URL text field
    $cmb->add_field( array(
        'name'  =>  __( 'Website URL', 'skinny_ninjah'),
        'desc'  =>  __( 'Video URL', 'skinny_ninjah'),
        'id'    =>  $prefix, 'url',
        'type'  =>  'text_url',
    ));
}