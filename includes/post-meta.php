<?php
/**
 * Created by PhpStorm.
 * User: thulani-matshoba
 * Date: 2019-03-24
 * Time: 00:55
 */


use Carbon_Fields\Container;
use Carbon_Fields\Field;
use Carbon_Fields\Block;

add_action( 'carbon_fields_register_fields', 'skinny_ninjah_post_meta' );
function skinny_ninjah_post_meta() {

    Block::make( __( 'Skinny Ninjah Block' ) )
        ->add_fields( array(
            Field::make( 'text', 'heading', __( 'Block Heading' ) ),
            Field::make( 'image', 'image', __( 'Block Image' ) ),
            Field::make( 'rich_text', 'content', __( 'Block Content' ) ),
        ) )
        ->set_icon( 'heart' )
        ->set_description( __( 'A skinny ninjah block consisting of a heading, an image and a text content.' ) )
        ->set_render_callback( function ( $block ) {
            ?>

            <div class="block">
                <div class="block__heading">
                    <h1><?php echo esc_html( $block['heading'] ); ?></h1>
                </div><!-- /.block__heading -->

                <div class="block__image">
                    <?php echo wp_get_attachment_image( $block['image'], 'full' ); ?>
                </div><!-- /.block__image -->

                <div class="block__content">
                    <?php echo apply_filters( 'the_content', $block['content'] ); ?>
                </div><!-- /.block__content -->
            </div><!-- /.block -->

            <?php
        } );

        Block::make( 'Skinny Ninjah Flight' )
        ->add_fields( [
         Field::make( 'map', 'from' ),
         Field::make( 'date_time', 'departure_timestamp' ),
         Field::make( 'map', 'destination' ),
         Field::make( 'date_time', 'arrival_timestamp' ),
     ] )
     ->set_render_callback( function( $flight ) {
         ?>
            <p>
                Flight departs
                from <?php echo esc_html( $flight['from']['address'] ) ?> at
                <?php echo $flight['departure_timestamp'] ?>
                and arrives
                to <?php echo esc_html( $flight['destination']['address'] ) ?> at
                <?php echo $flight['arrival_timestamp'] ?>
            </p>
         <?php
     });
}

add_action( 'after_setup_theme', 'skinny_ninjah_load' );



function skinny_ninjah_load() {
    //require_once( ABSPATH . '/vendor/autoload.php' );
    \Carbon_Fields\Carbon_Fields::boot();
}