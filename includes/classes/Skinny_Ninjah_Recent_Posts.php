<?php
/**
 * Created by PhpStorm.
 * User: thulani-matshoba
 * Date: 2019-03-17
 * Time: 21:09
 */


/*
 *  Recent posts widget with post thumbnails
*/
class Skinny_Ninjah_Recent_Posts extends WP_Widget_Recent_Posts {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct('skinny_ninjah_recent_posts', __('Skinny Ninjah Recent Posts') );
    }

    public function widget($args, $instance) {
        $cache = array();
        if ( ! $this->is_preview() ) {
            $cache = wp_cache_get( 'widget_recent_posts', 'widget' );
        }

        if ( ! is_array( $cache ) ) {
            $cache = array();
        }

        if ( ! isset( $args['widget_id'] ) ) {
            $args['widget_id'] = $this->id;
        }

        if ( isset( $cache[ $args['widget_id'] ] ) ) {
            echo $cache[ $args['widget_id'] ];
            return;
        }

        ob_start();

        $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts' );

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

        $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
        if ( ! $number )
            $number = 5;
        $show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

        /**
         * Filter the arguments for the Recent Posts widget.
         *
         * @since 3.4.0
         *
         * @see WP_Query::get_posts()
         *
         * @param array $args An array of arguments used to retrieve the recent posts.
         */
        $r = new WP_Query( apply_filters( 'widget_posts_args', array(
            'posts_per_page'      => $number,
            'no_found_rows'       => true,
            'post_status'         => 'publish',
            'ignore_sticky_posts' => true
        ) ) );

        if ($r->have_posts()) :
            ?>
            <?php echo $args['before_widget']; ?>
            <?php if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
        } ?>
            <ul>
                <?php while ( $r->have_posts() ) : $r->the_post(); ?>
                    <li class="uk-margin-bottom">
                        <a href="<?php the_permalink(); ?>"><?php
                            if ( has_post_thumbnail() ){
                                the_post_thumbnail('sidebar-thumb',  array('class' => 'uk-float-left uk-margin-small-right'));
                            } else{ ?>
                                <img class="uk-float-left uk-margin-small-right" width="120" height="80" src="<?php echo get_template_directory_uri(); ?>/src/images/ninjah.png"/>
                            <?php }
                            ?></a>
                        <a class="sidebar-title" href="<?php the_permalink(); ?>"><?php get_the_title(); ?><?php echo skinny_ninjah_title(45); ?></a>
                        <?php if ( $show_date ) : ?>
                            <span class="post-date"><?php echo get_the_date(); ?></span>
                        <?php endif; ?>
                    </li>
                <?php endwhile; ?>
            </ul>
            <?php echo $args['after_widget']; ?>

            <?php
            // Reset the global $the_post as this query will have stomped on it
            wp_reset_postdata();
            endif;

        if ( ! $this->is_preview() ) {
            $cache[ $args['widget_id'] ] = ob_get_flush();
            wp_cache_set( 'widget_recent_posts', $cache, 'widget' );
        } else {
            ob_end_flush();
        }
    }

} // class Skinny_Ninjah_Recent_Posts
// register Skinny_Ninjah_Recent_Posts
function register_Skinny_Ninjah_Recent_Posts() {
    register_widget( 'Skinny_Ninjah_Recent_Posts' );
}
add_action( 'widgets_init', 'register_Skinny_Ninjah_Recent_Posts' );