<?php
/**
 * Created by PhpStorm.
 * User: thulani-matshoba
 * Date: 2019-03-14
 * Time: 12:58
 */

if ( ! function_exists( 'skinny_ninjah_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function skinny_ninjah_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Skinny Ninjah, use a find and replace
         * to change 'skinny-ninjah' to the name of your theme in all the template files.
         */
        load_theme_textdomain( 'skinny-ninjah', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );
        
        // $options = get_option( 'post-formats' );
        // $formats = array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'audio', 'video', 'chat' );
        // $output = array();
        // foreach ( $formats as $format ) {
        //     $output[] = ( @options[$format] == 1 ? $format : '' );
        // }
        // if( !empty( $options ) ) {
        //     add_theme_support( 'post-formats', $output );
        // }
        
        add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'audio', 'video', 'chat' ));

        /**
         * Yoast Breadcrumbs
         */
        add_theme_support( 'yoast-seo-breadcrumbs' );

        /**
         * Thumbnail  Custom Sizes
         */
        add_image_size('featured-thumb', 600, 320, array('center', 'center'));
        add_image_size('featured-square', 300, 300, array('center', 'center'));
        add_image_size('featured-featured', 1240, 660, array('center', 'center'));
        add_image_size('sidebar-thumb', 120, 80, array('center', 'center'));
        add_image_size('logo-carousel', 120, 120, array('center', 'center'));


        // This theme uses wp_nav_menu() in one location.
        register_nav_menus( array(
            'header' => esc_html__( 'Primary Menu', 'skinny-ninjah' ),
            'social' => esc_html__( 'Social Menu', 'skinny-ninjah' ),
            'offcanvas' => esc_html__( 'Off Canvas Menu', 'skinny-ninjah' ),
        ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );

        $header = get_option( 'custom_header' );
        if( @$header == 1 ) {
            add_theme_support( 'custom-header' );
        }

        // Set up the WordPress core custom background feature.
        $background = get_option( 'custom_background' );
        if ( @$background == 1 ) {
            add_theme_support( 'custom-background', apply_filters( 'skinny_ninjah_custom_background_args', array(
                'default-color' => 'ffffff',
                'default-image' => '',
            ) ) );
        }

        // Add theme support for selective refresh for widgets.
        add_theme_support( 'customize-selective-refresh-widgets' );

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support( 'custom-logo', array(
            'height'      => 250,
            'width'       => 250,
            'flex-width'  => true,
            'flex-height' => true,
        ) );
    }
endif;
add_action( 'after_setup_theme', 'skinny_ninjah_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function skinny_ninjah_content_width() {
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters( 'skinny_ninjah_content_width', 640 );
}
add_action( 'after_setup_theme', 'skinny_ninjah_content_width', 0 );

/**
 * Removes the version string from js & css but only the ones generated by WP
 */
function skinny_ninjah_wp_version_strings( $src ) {
    global $wp_version;
    parse_str( parse_url($src, PHP_URL_QUERY), $query );
    if ( !empty( $query['ver'] ) && $query['ver'] === $wp_version ) {
        $src = remove_query_arg( 'ver', $src );
    }
    return $src;
}
add_filter( 'script_loader_src', 'skinny_ninjah_wp_version_strings');
add_filter( 'style_loader_src', 'skinny_ninjah_wp_version_strings');

/**
 * Removes meta tag generator from header
 */
function skinny_ninjah_remove_meta_version() {
    return '';
}
add_filter( 'the_generator', 'skinny_ninjah_remove_meta_version' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function skinny_ninjah_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Header Cart', 'skinny-ninjah' ),
        'id'            => 'header-cart',
        'description'   => esc_html__( 'Add widgets here.', 'skinny-ninjah' ),
        'before_widget' => '<section id="%1$s" class="ninjah-widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Home Page Slider', 'skinny-ninjah' ),
        'id'            => 'home-slider',
        'description'   => esc_html__( 'Add widgets here.', 'skinny-ninjah' ),
        'before_widget' => '<section id="%1$s" class="ninjah-widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Primary Sidebar', 'skinny-ninjah' ),
        'id'            => 'primary-sidebar',
        'description'   => esc_html__( 'Add widgets here.', 'skinny-ninjah' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Secondary Sidebar', 'skinny-ninjah' ),
        'id'            => 'secondary-sidebar',
        'description'   => esc_html__( 'Add widgets here.', 'skinny-ninjah' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'skinny_ninjah_widgets_init' );

/**
 * @param $html
 * @return mixed
 * This filter changes the class on the custom header logo
 */
function skinny_ninjah_custom_logo ( $html ) {
    $html = str_replace('custom-logo', 'sn_logo', $html);
    return $html;
}

add_filter('get_custom_logo', 'skinny_ninjah_custom_logo', 10);


/**
 * @param $limit
 * This is a filter to show a limited number of words that i title can show
 */
function skinny_ninjah_title( $limit ) {

    global $post;

    $title = get_the_title($post->ID);
    if( strlen( $title ) > $limit) {
        $title = substr( $title, 0, $limit ) . '...';
    }

    echo $title;

}

function skinny_ninjah_excerpt_length( $length ) {
    return 100;
}
add_filter( 'excerpt_length', 'skinny_ninjah_excerpt_length', 999 );


/**
 * Change administration panel footer
 */
function skinny_ninjah_footer_admin() {
    echo 'For Support, Please Call 076 719 5285 or email Thulani Matshoba here <a href="mailto:thulani90.m@gmail.com">thulani90.m@gmail.com</a>';
}
add_filter('admin_footer_text', 'skinny_ninjah_footer_admin');

/**
 * @param $text
 * @return bool|string
 * Trimmed Excerpt
 */
// function tm_length_excerpt( $text ) {
//     if( is_admin() ) {
//         return $text;
//     }
//     $read_more = '&hellip; <a class="read-more-link uk-button uk-button-default" href="' . get_the_permalink() . '">Read more</a>';

//     // Fetch the post content directly
//     $text = get_the_content();
//     // Clear out shortcodes
//     $text = strip_shortcodes( $text );

//     // Get the first 140 characteres
//     $text = substr( $text, 0, 120 );

//     // Add a read more tag
//     $text .= $read_more;
//     return $text;
// }
// // Leave priority at default of 10 to allow further filtering
// add_filter( 'wp_trim_excerpt', 'tm_length_excerpt', 10, 1 );

/**
 * @param $mimes
 * @return mixed
 *  This allows you to upload SVG's
 */
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


/**
 *
 */
function skinny_ninjah_pagination() {
    global $wp_query;
    $big = 999999999; // need an unlikely integer
    echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages
    ) );
}

function skinny_ninjah_get_attachment( $num = 1 ) {

    $output = '';
    if( has_post_thumbnail() && $num == 1 ) : 
        $output = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ); 
    else:
        $attachments = get_posts( array(
            'post_type' => 'attachment',
            'posts_per_page' => $num,
            'post_parent' => get_the_ID()
        ));
        if( $attachments && $num == 1 ) :
            foreach ( $attachments as $attachment ):
                $output = wp_get_attachment_url( $attachment->ID );
            endforeach;
            elseif( $attachments && $num > 1 ):
                $output = $attachments;
        endif;
        wp_reset_postdata(); 
    endif;

    return $output;
}

// Video Post Format 
function skinny_ninjah_get_embedded_media( $type = array() ) {
    $content = do_shortcode( apply_filters( 'the_content', get_the_content() ) );
        $embed = get_media_embedded_in_content( $content, $type );

        if( in_array( 'audio', $type ) ):
            $output = str_replace( '?visual=true', '?visual=false', $embed[0] );
        else:
            $output = $embed[0];
        endif;

        return $output;
}

//Gallery Post Format image slides
function skinny_ninjah_get_bs_slides( $attachments ) {

    $output = array();
    $count = count($attachments)-1;

    for( $i = 0; $i <= $count; $i++ ):
        $active  = ( $i == 0 ? ' active' : '' );
        $n       = ( $i == $count ? 0 : $i+1 );
        $nextImg = wp_get_attachment_thumb_url( $attachments[$i]->ID );
        $p       = ( $i == 0 ? $count : $i-1 );
        $prevImg = wp_get_attachment_thumb_url( $attachments[$i]->ID );

        $output[$i] = array(
            'class'     => $active,
            'url'       => wp_get_attachment_url( $attachments[$i]->ID ),
            'next_img'  => $nextImg,
            'prev_img'  => $prevImg,
            'caption'   => $attachments[$i]->post_excerpt
        );

    endfor;

    return $output;
}

function skinny_ninjah_grab_url() {
    if( ! preg_match( '/<a\s[^>]*?href=[\'"](.+?)[\'"]/i', get_the_content(), $links ) ) {
        return false;
    }
    return esc_url_raw( $links[1] );
}

function skinny_ninjah_grab_current_url() {
    $http = ( isset( $_SERVER["HTTPS"] ) ? 'https://' : 'http://' );
    $referer = $http . $_SERVER["HTTP_HOST"];
    $archive_url = $referer . $_SERVER["REQUEST_URI"];

    return $archive_url;
}

//This grabs the first uploaded image in a post
function skinny_ninjah_placeholder_image() {
    $files = get_children('post_parent='.get_the_ID().'&post_type=attachment
    &post_mime_type=image&order=desc');
      if($files) :
        $keys = array_reverse(array_keys($files));
        $j=0;
        $num = $keys[$j];
        $image=wp_get_attachment_image($num, 'large', true);
        $imagepieces = explode('"', $image);
        $imagepath = $imagepieces[1];
        $main=wp_get_attachment_url($num);
            $template=get_template_directory();
            $the_title=get_the_title();
        print "<img src='$main' alt='$the_title' class='featured-thumb' />";
      endif;
    }


/** Mail Trap www.mailtrap.io */

// function mailtrap($phpmailer) {
//     $phpmailer->isSMTP();
//     $phpmailer->Host = 'smtp.mailtrap.io';
//     $phpmailer->SMTPAuth = true;
//     $phpmailer->Port = 2525;
//     $phpmailer->Username = '0c7a61171985cb';
//     $phpmailer->Password = '0bc125e71ad15f';
//   }
  
//   add_action('phpmailer_init', 'mailtrap');



//Track Post Views
function skinny_ninjah_getPostViews( $postID )
{
    $count_key = 'post_views_count';
    $count = get_post_meta( $postID, $count_key, true );
    if ( $count == '' ) {
        delete_post_meta( $postID, $count_key );
        add_post_meta( $postID, $count_key, '0' );
        return "0 View";
    }
    return $count . ' Views';
}

function skinny_ninjah_setPostViews( $postID )
{
    $count_key = 'post_views_count';
    $count = get_post_meta( $postID, $count_key, true );
    if ( $count == '' ) {
        $count = 0;
        delete_post_meta( $postID, $count_key );
        add_post_meta( $postID, $count_key, '0' );
    } else {
        $count++;
        update_post_meta( $postID, $count_key, $count );
    }
}