<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Skinny_Ninjah
 */


function skinny_ninjah_posted_meta() {
	$posted_on = human_time_diff( get_the_time('U'), current_time('timestamp') );
	$categories = get_the_category();
	$separator = ', ';
	$output = '';
	$i = 1;

	if( !empty($categories) ) :
		foreach( $categories as $category ) :
			if( $i > 1 ) : $output .= $separator; endif;
			$output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( 'View all posts in%s', $category->name ) . '">' . esc_html( $category->name ) . '</a>';
		$i++; 
	endforeach;
	endif;
	
	return '<span class="posted-on uk-text-small uk-text-muted">Posted <a href="'. esc_url( get_permalink() ) .'">' . $posted_on . '</a> ago</span> / <span class="posted-in">' . $output  . '</span>';
}

function skinny_ninjah_posted_footer() {
	$comments_num = get_comments_number();
	if( comments_open() ) {
		if( $comments_num == 0 ) {
			$comments = __('No Comments');
		} elseif ( $comments_num > 1 ) {
			$comments = $comments_num . __(' Comments');
		} else {
			$comments = __('1 Comment');
		}
		$comments = '<a href="' . get_comments_link() . '">'. $comments . '</a>';

	} else {
		$comments = __('Comments are closed');
	}

	return '<div class="post-footer-container uk-padding-top">'. get_the_tag_list('<div class="uk-float-left"><span class="skinny-ninjah-icon skinny-ninjah-price-tag"></span>', ' ', '</div>') .'<div class="uk-float-right"><span class="skinny-ninjah-icon skinny-ninjah-bullhorn"></span>'. $comments .'</div>';
}




if ( ! function_exists( 'skinny_ninjah_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function skinny_ninjah_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published uk-text-small uk-text-muted" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( ' | %s', 'post date', 'skinny-ninjah' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<span class="posted-on uk-text-small uk-text-muted">' . $posted_on . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'skinny_ninjah_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function skinny_ninjah_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'By: %s', 'post author', 'skinny-ninjah' ),
			'<span class="author vcard"><a class="url fn n uk-text-small uk-text-muted" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline uk-text-small uk-text-muted"> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'skinny_ninjah_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function skinny_ninjah_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'skinny-ninjah' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( 'Posted in: %1$s', 'skinny-ninjah' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'skinny-ninjah' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( ' Tagged: %1$s', 'skinny-ninjah' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'skinny-ninjah' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'skinny-ninjah' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'skinny_ninjah_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function skinny_ninjah_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">

                <?php if ( has_post_thumbnail() ) {
                    the_post_thumbnail('featured-featured', array('class' => 'single-thumb'));
                } else { ?>
					<img width="600" height="320" src="<?php echo get_template_directory_uri(); ?>/src/images/ninjah.png"/>
                <?php } ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

		<div class="post-thumbnail">

		<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
		<?php if ( has_post_thumbnail() ) {
                    the_post_thumbnail('featured-thumb', array('class' => 'single-thumb'));
                } else { ?>
					<img width="600" height="320" src="<?php echo get_template_directory_uri(); ?>/src/images/ninjah.png"/>
                <?php } ?>
		</a>
		</div>

		<?php
		endif; // End is_singular().
	}
endif;
