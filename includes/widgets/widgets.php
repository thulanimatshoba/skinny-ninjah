<?php

use Carbon_Fields\Widget;
use Carbon_Fields\Field;


add_action( 'carbon_fields_register_fields', 'skinny_ninjah_widget' );
function skinny_ninjah_widget()
{
    class SkinnyNinjahWidget extends Widget
    {
        // Register widget function. Must have the same name as the class
        function __construct()
        {
            $this->setup('skinny_ninjah_widget', 'SN Widget', 'Displays a block with title/text', array(
                Field::make('text', 'title', 'Title')->set_default_value('Hello World!'),
                Field::make('rich_text', 'content', 'Content')->set_default_value('Lorem Ipsum dolor sit amet'),

                Field::make( 'complex', 'crb_slider', __( 'Slider' ) )
                    ->add_fields( array(
                        Field::make( 'text', 'title', __( 'Slide Title' ) ),
                        Field::make( 'image', 'photo', __( 'Slide Photo' ) ),
                    ) )
            ));
        }

        // Called when rendering the widget in the front-end
        function front_end($args, $instance)
        {
            echo $args['before_title'] . $instance['title'] . $args['after_title'];
            echo '<p>' . $instance['content'] . '</p>';
        }
    }
}

add_action( 'after_setup_theme', 'skinny_ninjah_load' );



function load_widgets() {
    register_widget( 'SkinnyNinjahWidget' );
}
add_action( 'widgets_init', 'load_widgets' );


//-----------------------------------------------------------


//Ninjah Profile Widget

class Skinny_Ninjah_Profile_Widget extends WP_Widget {

	//setup the widget name, description, etc...
	public function __construct() {

		$widget_ops = array(
			'classname' => 'skinny-ninjah-profile-widget',
			'description' => 'Custom Skinny Ninjah Profile Widget',
		);
		parent::__construct( 'skinny_ninjah_profile', 'Skinny Ninjah Profile', $widget_ops );

	}

	//back-end display of widget
	public function form( $instance ) {
		echo '<p><strong>No options for this Widget!</strong><br/>You can control the fields of this Widget from <a href="./admin.php?page=skinny_ninjah">This Page</a></p>';
	}

	//front-end display of widget
	public function widget( $args, $instance ) {

		$picture = esc_attr( get_option( 'profile_picture' ) );
		$firstName = esc_attr( get_option( 'first_name' ) );
		$lastName = esc_attr( get_option( 'last_name' ) );
		$fullName = $firstName . ' ' . $lastName;
		$description = esc_attr( get_option( 'user_description' ) );

		$twitter_icon = esc_attr( get_option( 'twitter_handler' ) );
		$facebook_icon = esc_attr( get_option( 'facebook_handler' ) );
    $gplus_icon = esc_attr( get_option( 'gplus_handler' ) );
		$instagram_icon = esc_attr( get_option( 'instagram_handler' ) );


		echo $args['before_widget'];
		?>
		<div class="text-center">
			<div class="image-container">
				<div id="profile-picture-preview" class="profile-picture uk-background-cover uk-height-small" style="background-image: url(<?php print $picture; ?>); "></div>
			</div>
			<h2 class="widget-title skinny-ninjah-username"><?php print $fullName; ?></h2>
			<p class="skinny-ninjah-description"><?php print $description; ?></p>
      <hr style="margin: 8px 0;">
			<div class="icons-wrapper">
				<?php if( !empty( $twitter_icon ) ): ?>
					<a href="https://twitter.com/<?php echo $twitter_icon; ?>" target="_blank"><span class="skinny-ninjah-icon-sidebar skinny-ninjah-icon skinny-ninjah-twitter"></span></a>
				<?php endif;
				if( !empty( $gplus_icon ) ): ?>
					<a href="https://plus.google.com/u/0/+<?php echo $gplus_icon; ?>" target="_blank"><span class="skinny-ninjah-icon-sidebar skinny-ninjah-icon skinny-ninjah-googleplus"></span></a>
				<?php endif;
				if( !empty( $facebook_icon ) ): ?>
					<a href="https://facebook.com/<?php echo $facebook_icon; ?>" target="_blank"><span class="skinny-ninjah-icon-sidebar skinny-ninjah-icon skinny-ninjah-facebook2"></span></a>
                <?php endif;
                if( !empty( $instagram_icon ) ): ?>
					<a href="https://instagram.com/<?php echo $instagram_icon; ?>" target="_blank"><span class="skinny-ninjah-icon-sidebar skinny-ninjah-icon skinny-ninjah-instagram"></span></a>
				<?php endif; ?>
			</div>
      <hr style="margin-top: 8px;">
		</div>
		<?php
		echo $args['after_widget'];
	}

}

add_action( 'widgets_init', function() {
	register_widget( 'Skinny_Ninjah_Profile_Widget' );
} );


/*
	Edit default WordPress widgets
*/

function skinny_ninjah_tag_cloud_font_change( $args ) {

	$args['smallest'] = 8;
	$args['largest'] = 8;

	return $args;

}
add_filter( 'widget_tag_cloud_args', 'skinny_ninjah_tag_cloud_font_change' );

function skinny_ninjah_list_categories_output_change( $links ) {

	$links = str_replace('</a> (', '</a> <span>', $links);
	$links = str_replace(')', '</span>', $links);

	return $links;

}
add_filter( 'wp_list_categories', 'skinny_ninjah_list_categories_output_change' );


/** Popular Posts Widget */

class Skinny_Ninjah_Porpular_Posts_Widget extends WP_Widget {

    public function __construct() {

		$widget_ops = array(
			'classname' => 'skinny-ninjah-popular-posts-widget',
			'description' => 'Popular Posts Widget',
		);
		parent::__construct( 'skinny_ninjah_popular_posts', 'Skinny Ninjah Porpular Posts', $widget_ops );

    }

    //back-end display of widget
    public function form( $instance ) {
        $title = ( !empty( $instance[ 'title' ] ) ? $instance[ 'title' ] : 'Porpular Posts' );
        $tot = ( !empty( $instance[ 'tot' ] ) ? absint( $instance[ 'tot' ] ) : 4 );

        $output = '<p>';
        $output .= '<label for="' . esc_attr( $this->get_field_id( 'title' ) ) . '">Title:</label>';
        $output .= '<input type="text" class="widefat" id="' . esc_attr( $this->get_field_id( 'title' ) ) . '" name="'. esc_attr( $this->get_field_name( 'title' ) ) . '" value="' . esc_attr( $title ) . '"';
        $output .= '</p>';

        $output .= '<p>';
        $output .= '<label for="' . esc_attr( $this->get_field_id( 'tot' ) ) . '">Number of posts:</label>';
        $output .= '<input type="number" class="widefat" id="' . esc_attr( $this->get_field_id( 'tot' ) ) . '" name="'. esc_attr( $this->get_field_name( 'tot' ) ) . '" value="' . esc_attr( $tot ) . '"';
        $output .= '</p>';

        echo $output;

    }

    //update widget
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance[ 'title' ] = ( !empty( $new_instance[ 'title' ] ) ? strip_tags( $new_instance[ 'title' ] ) : '');
        $instance[ 'tot' ] = ( !empty( $new_instance[ 'tot' ] ) ? absint( strip_tags( $new_instance[ 'tot' ] ) ) : 0 );

        return $instance;
    }

    //front-end display of widget
    public function widget( $args, $instance ) {

        $tot = absint( $instance[ 'tot' ] );

        $posts_args = array(
            'post_type'         => 'post',
            'posts_per_page'    => $tot,
            'meta_key'          => 'skinny_ninjah_post_views',
            'orderby'           => 'meta_value_num',
            'order'             => 'DESC'
        );

        $posts_query = new WP_Query( $posts_args );

        echo $args[ 'before_widget' ];

        if( !empty( $instance[ 'title' ] ) ):
            echo $args[ 'before_title' ] . apply_filters( 'widget_title', $instance[ 'title' ] ) . $args[ 'after_title' ];
        endif;

        if( $posts_query->have_posts() ):
            //echo '<ul>';
                while( $posts_query->have_posts() ): $posts_query->the_post();
                    echo '<div class="media uk-margin">';
                    echo '<div class="media-left uk-float-left"><img class="media-object" src="' . get_template_directory_uri() . '/src/images/post-' . ( get_post_format() ? get_post_format() : 'standard' ) . '.png" alt="' . get_the_title() . '"/> </div>';
                    echo '<div class="media-body">';
                    echo '<a href="' . get_the_permalink() . '" title="' . get_the_title() . '">' . get_the_title() . '</a>';
                    echo '<div class="row"><div class="col-xs-12">'. skinny_ninjah_posted_footer( true ) .'</div></div>';
                    echo '</div>';
                    echo '</div>';

                endwhile;
            //echo '</ul>';

        endif;

        echo $args[ 'after_widget' ];

    }

}
add_action( 'widgets_init', function() {
    register_widget( 'Skinny_Ninjah_Porpular_Posts_Widget' );
});
