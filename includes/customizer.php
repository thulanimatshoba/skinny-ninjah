<?php
/**
 * Skinny Ninjah Theme Customizer
 *
 * @package Skinny_Ninjah
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function skinny_ninjah_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	$wp_customize->add_setting('tm_link_color', array(
        'default' => '#006ec3',
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting('tm_nav_color', array(
        'default' => '#006ec3',
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting('tm_btn_color', array(
        'default' => '#006ec3',
        'transport' => 'refresh',
    ));

    $wp_customize->add_setting('tm_btn_hover_color', array(
        'default' => '#004C87',
        'transport' => 'refresh',
    ));

    $wp_customize->add_section('tm_standard_colors', array(
        'title' => __('Standard Colors', 'thulani-matshoba'),
        'priority' => 30,
    ));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tm_link_color_control', array(
        'label' => __('Link Color', 'thulani-matshoba'),
        'section' => 'tm_standard_colors',
        'settings' => 'tm_link_color',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tm_nav_color_control', array(
        'label' => __('Nav Color', 'thulani-matshoba'),
        'section' => 'tm_standard_colors',
        'settings' => 'tm_nav_color',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tm_btn_color_control', array(
        'label' => __('Button Color', 'thulani-matshoba'),
        'section' => 'tm_standard_colors',
        'settings' => 'tm_btn_color',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'tm_btn_hover_color_control', array(
        'label' => __('Button Hover Color', 'thulani-matshoba'),
        'section' => 'tm_standard_colors',
        'settings' => 'tm_btn_hover_color',
    ) ) );




    //Footer Callout

    $wp_customize->add_section('tm-footer-callout-section', array(
        'title' => 'Footer Callout'
    ));

    $wp_customize->add_setting('tm-footer-callout-display', array(
        'default' => 'No'
    ));

    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'tm-footer-callout-display-control', array(
        'label' => 'Display this section?',
        'section' => 'tm-footer-callout-section',
        'settings' => 'tm-footer-callout-display',
        'type' => 'select',
        'choices' => array('No' => 'No', 'Yes' => 'Yes')
    )));

    $wp_customize->add_setting('tm-footer-callout-headline', array(
        'default' => 'Example Headline Text!'
    ));

    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'tm-footer-callout-headline-control', array(
        'label' => 'Headline',
        'section' => 'tm-footer-callout-section',
        'settings' => 'tm-footer-callout-headline'
    )));

    $wp_customize->add_setting('tm-footer-callout-text', array(
        'default' => 'Example paragraph text.'
    ));

    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'tm-footer-callout-text-control', array(
        'label' => 'Text',
        'section' => 'tm-footer-callout-section',
        'settings' => 'tm-footer-callout-text',
        'type' => 'textarea'
    )));

    $wp_customize->add_setting('tm-footer-callout-link');

    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'tm-footer-callout-link-control', array(
        'label' => 'Link',
        'section' => 'tm-footer-callout-section',
        'settings' => 'tm-footer-callout-link',
        'type' => 'dropdown-pages'
    )));

    $wp_customize->add_setting('tm-footer-callout-image');

    $wp_customize->add_control( new WP_Customize_Cropped_Image_Control($wp_customize, 'tm-footer-callout-image-control', array(
        'label' => 'Image',
        'section' => 'tm-footer-callout-section',
        'settings' => 'tm-footer-callout-image',
        'width' => 750,
        'height' => 500
    )));

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'skinny_ninjah_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'skinny_ninjah_customize_partial_blogdescription',
		) );
	}
}
add_action( 'customize_register', 'skinny_ninjah_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function skinny_ninjah_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function skinny_ninjah_customize_partial_blogdescription() {
	bloginfo( 'description' );
}


/** SN Theme Settings  **/

$sn_config = 'sn_config';

//Master Panel
$sn_master_panel = 'sn_master_panel';

/**  Ninjah Sections  **/

$sn_header_section = 'sn_header_section';
$sn_header_top_section = 'sn_header_top_section';
$sn_header_bottom_section = 'sn_header_bottom_section';
$sn_top_footer_section = 'sn_top_footer_section';
$sn_bottom_footer_section = 'sn_bottom_footer_section';
$sn_typography_section = 'sn_typography_section';
$sn_dimensions_section = 'sn_dimensions_section';
$sn_front_page_section = 'sn_front_page_section';
$sn_article_page_section = 'sn_article_page_section';


/** Ninjah Panels */
$sn_header_panel = 'sn_header_panel';
$sn_footer_panel = 'sn_footer_panel';
$sn_typography_panel = 'sn_typography_panel';
$sn_layout_panel = 'sn_layout_panel';


Kirki::add_panel( $sn_master_panel, array(
    'title' => esc_html__( 'SN Theme Options', 'skinny-ninjah' ),
));

Kirki::add_panel( $sn_header_section, array(
    'title' => 'Header Options',
    'panel' => $sn_master_panel,
));

/** https://infismash.com/extending-the-wordpress-customizer-using-kirki-4242e0e8da2b */

Kirki::add_panel( $sn_layout_panel, array(
    'title' => esc_html__( 'Layout Options', 'skinny-ninjah' ),
    'panel' => $sn_master_panel,
) );

Kirki::add_panel( $sn_footer_panel, array(
    'title' => esc_html__( 'Footer Options', 'skinny-ninjah' ),
    'panel' => $sn_master_panel,
));









/** Sections  **/

Kirki::add_section( $sn_header_top_section, array(
    'title' => esc_html__( 'Header Top Section', 'skinny-ninjah' ),
    'panel' => $sn_header_section,
));

Kirki::add_section( $sn_header_bottom_section, array(
    'title' => esc_html__( 'Header Bottom Section', 'skinny-ninjah' ),
    'panel' => $sn_header_section,
));

Kirki::add_section( $sn_top_footer_section, array(
    'title' => esc_html__( 'Footer Top Section', 'skinny-ninjah' ),
    'panel' => $sn_footer_panel,
));

Kirki::add_section( $sn_bottom_footer_section, array(
    'title' => esc_html__( 'Footer Bottom Section', 'skinny-ninjah' ),
    'panel' => $sn_footer_panel,
));

Kirki::add_section( $sn_dimensions_section, array(
    'title'          => esc_html__( 'Dimensions', 'skinny-ninjah' ),
    'description'    => esc_html__( 'Choose the container size', 'skinny-ninjah' ),
    'panel'          => $sn_layout_panel,
) );


Kirki::add_section( $sn_front_page_section, array(
    'title'          => esc_html__( 'Home Page', 'skinny-ninjah' ),
    'description'    => esc_html__( 'Home Page Layout Options', 'skinny-ninjah' ),
    'panel'          => $sn_layout_panel,
) );


Kirki::add_section( $sn_article_page_section, array(
    'title'          => esc_html__( 'Article Page', 'skinny-ninjah' ),
    'description'    => esc_html__( 'Article page layout options', 'skinny-ninjah' ),
    'panel'          => $sn_layout_panel,
) );


Kirki::add_section( $sn_typography_section, array(
    'title'   => esc_html__( 'Typography', 'skinny-ninjah' ),
    'panel' => $sn_master_panel,
));




/** Fields */

 foreach ( array( $sn_top_footer_section, $sn_bottom_footer_section, $sn_header_top_section, $sn_header_bottom_section, $sn_article_page_section ) as $section ) {

    Kirki::add_field( $sn_config, array(
        'type'     => 'toggle',
        'settings' => $section . '_show_setting',
        'section'  => $section,
        'label'    => esc_html__( 'Show this section', 'skinny-ninjah' ),
        'default'  => '1',
        'priority' => 10,
    ) );
}


Kirki::add_field( $sn_config, array(
   'type'        => 'background',
   'settings'    => 'body_background',
   'label'       => esc_html__( 'Choose your header background', 'skinny-ninjah' ),
   'description' => esc_html__( 'The background you specify here will apply to the top footer section.', 'skinny-ninjah' ),
   'section'     => $sn_top_footer_section,
   'default'     => array(
       'color'    => '#808080',
       'image'    => '',
       'repeat'   => 'no-repeat',
       'size'     => 'cover',
       'attach'   => 'fixed',
       'position' => 'left-top',
   ),
   'priority'    => 10,
   'output'      => '#masthead'
) );

Kirki::add_field( $sn_config, array(
	'type'     => 'select',
	'settings' => 'my_setting',
	'label'    => esc_html__( 'Select a post', 'kirki' ),
	'section'  => $sn_front_page_section,
	'default'  => 'option-1',
	'priority' => 10,
	'multiple' => 1,
	'choices'  => Kirki_Helper::get_terms( 'product_cat' ),
) );


Kirki::add_field( $sn_config, array(
    'type'        => 'color',
    'settings'    => 'top_footer_bg',
    'label'       => esc_html__( 'Color Control (hex-only)', 'skinny-ninjah' ),
    'description' => esc_html__( 'This is a color control - without alpha channel.', 'skinny-ninjah' ),
    'section'     => $sn_top_footer_section,
    'default'     => '#0088CC',
    'choices' => array(
        'alpha' => true,
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '#top-footer-section',
            'property'  => 'background-color'
        ),
    ),
) );

Kirki::add_field( $sn_config, array(
   'type'        => 'background',
   'settings'    => 'body_background',
   'label'       => esc_html__( 'Choose your header background', 'skinny-ninjah' ),
   'description' => esc_html__( 'The background you specify here will apply to the top footer section.', 'skinny-ninjah' ),
   'section'     => $sn_top_footer_section,
   'default'     => array(
       'color'    => '#808080',
       'image'    => '',
       'repeat'   => 'no-repeat',
       'size'     => 'cover',
       'attach'   => 'fixed',
       'position' => 'left-top',
   ),
   'priority'    => 10,
   'output'      => '#masthead'
) );

Kirki::add_field( $sn_config, array(
    'type'      => 'slider',
    'setting'   =>  'container_width',
    'section'   => $sn_dimensions_section,
    'label'     =>  esc_html__( 'Select container width', 'skinny-ninjah' ),
    'default'   =>  980,
    'transport' => 'auto',
    'choices'   => [
        'min'   => '980px',
        'max'   => '1200px',
        'step'  => '20',
    ],
    'output'    => [
        [
            'element'    => '.sn-site-wrap',
            'property'   => 'max-width',
            'suffix'     => 'px',
        ]
    ]
));

/* adding layout_front_page_setting field */
Kirki::add_field( $sn_config, array(
    'type'        => 'radio-image',
    'settings'     => 'layout_front_page_setting',
    'label'       => esc_html__( 'Layout for the front page', 'skinny-ninjah' ),
    'section'     => $sn_front_page_section,
    'default'     => 'right-sidebar',
    'priority'    => 10,
    'choices'     => array(
        'right-sidebar' => get_template_directory_uri() . '/kirki/assets/images/2cr.png',
        'left-sidebar' => get_template_directory_uri() . '/kirki/assets/images/2cl.png',
        'full-width' => get_template_directory_uri() . '/kirki/assets/images/1c.png',
        'dual-sidebar' => get_template_directory_uri() . '/kirki/assets/images/3cm.png',
        'left-dual-sidebar' => get_template_directory_uri() . '/kirki/assets/images/3cl.png',
        'right-dual-sidebar' => get_template_directory_uri() . '/kirki/assets/images/3cr.png',
    ),
) );

/* adding layout_front_page_slider_image_one_setting field */
Kirki::add_field( $sn_config, array(
    'settings' => 'layout_front_page_slider_image_one_setting',
    'label'    => esc_html__( 'First image of the slider', 'skinny-ninjah' ),
    'section'  => $sn_front_page_section,
    'type'     => 'image',
    'priority' => 10,
    'default'  => '',
) );


Kirki::add_field( $sn_config, array(
    'type'        => 'typography',
    'settings'    => 'navigation_typography',
    'label'       => esc_html__( 'Navigation Typography', 'skinny-ninjah' ),
    'description' => esc_html__( 'Select the typography options for your menu.', 'skinny-ninjah' ),
    'help'        => esc_html__( 'The typography options you set here apply to all the paragraph and list content on your site.', 'skinny-ninjah' ),
    'section'     => $sn_typography_section,
    'priority'    => 10,
    'transport'    => 'auto',
    'default'     => array(
        'font-family'    => 'Open Sans Condensed',
        'variant'        => '400',
        'font-size'      => '15px',
        'line-height'    => '1.5',
        // 'letter-spacing' => '0',
        'color'          => '#fdd017',
    ),
    'output' => [
        [
            'element' => '.main-navigation a',
        ],
    ],
));

Kirki::add_field( $sn_config, array(
    'type'        => 'typography',
    'settings'    => 'body_typography',
    'label'       => esc_html__( 'Body Typography', 'skinny-ninjah' ),
    'description' => esc_html__( 'Select the typography options for your body.', 'skinny-ninjah' ),
    'section'     => $sn_typography_section,
    'priority'    => 10,
    'default'     => array(
        'font-family'    => 'Open Sans Condensed',
        'variant'        => '400',
        'font-size'      => '15px',
        'line-height'    => '1.5',
        // 'letter-spacing' => '0',
        'color'          => '#fdd017',
    ),
    'output' => [
        [
            'element' => 'p',
        ],
    ],
));


//Footer


Kirki::add_field( $sn_config, array(
    'type' => 'text',
    'settings' => 'footer_copyrights',
    'label' => __('Footer Copyrights'),
    'section' => $sn_bottom_footer_section,
    'transport'    => 'auto',
    'default' => 'Copyrights © ' . date('Y') . ' All rights reserved',
    'priority' => 10,
));

Kirki::add_field($sn_config, array(
    'type' => 'background',
    'settings' => 'bottom_footer_bg',
    'label' => __('Background Color'),
    'description' => __('The color you specify here will apply to the footer bottom section.'),
    'section' => $sn_bottom_footer_section,
    'default' => '#00000',
    'choices' => array(
        'alpha' => true,
    ),
    'priority' => 10,
    'transport'   => 'auto',
    'output' => [
        [
            'element' => '.footer-wrapper',
            'property'  => 'background-color'
        ],
    ],
));














/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function skinny_ninjah_customize_preview_js() {
	wp_enqueue_script( 'skinny-ninjah-customizer', get_template_directory_uri() . '/src/js/customizer.js', array( 'jquery', 'customize-preview' ), THULANI_VER, true );
}
add_action( 'customize_preview_init', 'skinny_ninjah_customize_preview_js' );
