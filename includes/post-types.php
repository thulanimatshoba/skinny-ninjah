<?php
/**
 * Created by PhpStorm.
 * User: thulani-matshoba
 * Date: 2019-03-24
 * Time: 00:58
 */

function create_post_type() {

    register_post_type( 'recipes',
        // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Recipes' ),
                'singular_name' => __( 'Recipe', 'skinny-ninjah' ),
                'all_items' => __( 'All Recipes', 'skinny-ninjah' ),

            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'recipes',
            'pages' => true),
            'menu_icon' => 'dashicons-carrot',
            'hierarchical' => true,
            'taxonomies' => array('post_tag' => 'Difficulty'),
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_post_type' );




//Admin

$contact = get_option( 'activate_contact' );
if( @$contact == 1 ) {
    add_action( 'init', 'skinny_ninjah_contact_custom_post_type' );

    add_filter( 'manage_sn-contact_posts_columns', 'skinny_ninjah_set_contact_columns' );
    add_action( 'manage_sn-contact_posts_custom_column', 'skinny_ninjah_contact_custom_column', 10, 2 );

    add_action( 'add_meta_boxes', 'skinny_ninjah_contact_add_meta_box' );
    add_action( 'save_post', 'skinny_ninjah_save_contact_email_data' );
}

/* Contact Custom Post Type*/
function skinny_ninjah_contact_custom_post_type() {
    $labels = array(
        'name'              => 'Messages',
        'singular_name'     => 'Message',
        'menu_name'         => 'Messages',
        'name_admin_bar'    => 'Message'
    );

    $args = array(
        'labels'            => $labels,
        'show_ui'           => true,
        'show_in_menu'      => true,
        'capabilitiy_type'  => 'post',
        'menu_position'     => 26,
        'menu_icon'         => 'dashicons-email-alt',
        'hierarchical'      => false,
        'supports'          => array( 'title', 'editor', 'author' )
    );

    register_post_type( 'sn-contact', $args );
}

function skinny_ninjah_set_contact_columns( $columns ) {
    $newColumns = array();
    $newColumns['title'] = 'Full Name';
    $newColumns['message'] = 'Message';
    $newColumns['email'] = 'Email';
    $newColumns['date'] = 'Date';
    return $newColumns;
}

function skinny_ninjah_contact_custom_column( $column, $post_id ) {

    switch( $column ) {
        case 'message' :
            echo get_the_excerpt();
            break;

        case 'email' :
            $email = get_post_meta( $post_id, '_contact_email_value_key', true );
            echo '<a href="mailto:'.$email.'">'.$email.'</a>';
            break;
    }
}

/* Contact Meta Boxes */
function skinny_ninjah_contact_add_meta_box() {
    add_meta_box( 'contact_email', 'User Email', 'skinny_ninjah_contact_email_callback', 'sn-contact', 'side' );
}

function skinny_ninjah_contact_email_callback( $post ) {
    wp_nonce_field( 'skinny_ninjah_save_contact_email_data', 'skinny_ninjah_contact_email_meta_box_nonce' );

    $value = get_post_meta( $post->ID, '_contact_email_value_key', true );

    echo '<label for="skinny_ninjah_contact_email_field">User Email Address: </label>';
    echo '<input type="email" id="skinny_ninjah_contact_email_field" name="skinny_ninjah_contact_email_field" value="' . esc_attr( $value ) . '" size="25" />';
}

function skinny_ninjah_save_contact_email_data( $post_id ) {

    if( ! isset( $_POST['skinny_ninjah_contact_email_meta_box_nonce'] ) ) {
        return;
    }

    if( ! wp_verify_nonce( $_POST['skinny_ninjah_contact_email_meta_box_nonce'], 'skinny_ninjah_save_contact_email_data' ) ) {
        return;
    }

    if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
        return;
    }

    if( ! current_user_can( 'edit_post', $post_id ) ) {
        return;
    }

    if( ! isset( $_POST['skinny_ninjah_contact_email_field'] ) ) {
        return;
    }

    $my_data = sanitize_text_field( $_POST['skinny_ninjah_contact_email_field'] );

    update_post_meta( $post_id, '_contact_email_value_key', $my_data );

}
