<?php
/**
 * Created by PhpStorm.
 * User: thulani-matshoba
 * Date: 2019-03-14
 * Time: 12:54
 */


/**
 * Enqueue scripts and styles.
 */
function skinny_ninjah_load_admin_scripts( $hook ) {
   if( 'toplevel_page_skinny_ninjah' != $hook ) {
       return;
   }

    wp_register_style( 'skinny_ninjah_admin', get_template_directory_uri() . '/assets/css/skinny-ninjah.admin.css', [], THULANI_VER );
    wp_enqueue_style( 'skinny_ninjah_admin' );

    wp_enqueue_media();
    
    wp_register_script( 'skinny-ninjah-admin-script', get_template_directory_uri() . '/assets/src/skinny-ninjah.admin.js', array('jquery'), THULANI_VER, true );
    wp_enqueue_script( 'skinny-ninjah-admin-script' );
}
add_action( 'admin_enqueue_scripts', 'skinny_ninjah_load_admin_scripts' );


function skinny_ninjah_scripts() {
    wp_enqueue_style( 'skinny-ninjah-style', get_stylesheet_uri() . '', [], THULANI_VER );
    wp_enqueue_style( 'skinny_ninjah_app', get_template_directory_uri() . '/dist/css/app.css', [], THULANI_VER );


    //Font Awesome
   wp_enqueue_style( 'font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' );

   wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');
   wp_enqueue_script( 'skinny-ninjah-app', get_template_directory_uri() . '/dist/app.js', [], THULANI_VER, true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'skinny_ninjah_scripts' );